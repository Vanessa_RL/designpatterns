using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVVMExample.ViewModels;

namespace MVVMTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NoValidarNombre()
        {
            JugadoresVM pr = new JugadoresVM();
            bool validado = pr.ValidaTexto("Jorg3");
            Assert.AreEqual(false, validado);
        }

        [TestMethod]
        public void ValidarNombre()
        {
            JugadoresVM pr = new JugadoresVM();
            bool validado = pr.ValidaTexto("Jorge");
            Assert.AreEqual(true, validado);
        }

        [TestMethod]
        public void NoValidarDorsal()
        {
            JugadoresVM pr = new JugadoresVM();
            bool validado = pr.ValidaDorsal(0);
            Assert.AreEqual(false, validado);
        }

        [TestMethod]
        public void NoValidarDorsalv2()
        {
            JugadoresVM pr = new JugadoresVM();
            bool validado = pr.ValidaDorsal(100);
            Assert.AreEqual(false, validado);
        }

        [TestMethod]
        public void ValidarDorsal()
        {
            JugadoresVM pr = new JugadoresVM();
            bool validado = pr.ValidaDorsal(45);
            Assert.AreEqual(true, validado);
        }

        [TestMethod]
        public void ValidarAddJugador()
        {
            JugadoresVM pr = new JugadoresVM();
            int rows = pr.Jugadores.Count;
            pr.AddToListaJugadores("Nombre Apellido Dorsal: 6");
            Assert.AreEqual(rows + 1, pr.Jugadores.Count);
        }
    }
}
