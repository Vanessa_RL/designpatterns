﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVMExample.Model;
using MVVMExample.Utils;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace MVVMExample.ViewModels
{
    public class JugadoresVM : ObservableObject
    {
        private readonly TextConverter _textConverter = new TextConverter(s => s.ToUpper());
        private string _nombre;
        private string _apellido;
        private int _dorsal;
        private readonly ObservableCollection<string> _jugadores  = new ObservableCollection<string>();

        #region Propiedades
        public string Nombre
        {
            get { return _nombre; }
            set
            {
                _nombre = value;
                RaisePropertyChangedEvent("Nombre");
            }
        }

      
        public string Apellido
        {
            get { return _apellido; }
            set
            {
                _apellido = value;
                RaisePropertyChangedEvent("Apellido");
            }
        }

        public int Dorsal
        {
            get { return _dorsal; }
            set
            {
                _dorsal = value;
                RaisePropertyChangedEvent("Dorsal");
            }
        }
        public ICollection<string> Jugadores
        {
            get { return _jugadores; }
        }
        #endregion

        public ICommand AddJugadorCommand
        {
            get { return new DelegateCommand(AddJugador); }
        }

        #region Validaciones
        //No se permiten caracteres numéricos en las cadenas (Nombre y Apellido)
        public bool ValidaTexto(string nombre)
        {
            if (String.IsNullOrEmpty(nombre) || Regex.IsMatch(nombre, @"\d"))
                return false;
            else
                return true;

        }

        //Solo se permiten dorsales entre el 1 y el 99
        public bool ValidaDorsal(int num)
        {
            if (num > 0 && num < 100)
                return true;
            else
                return false;

        }
        #endregion

        private void AddJugador()
        {
            if(!ValidaTexto(Nombre) || !ValidaTexto(Apellido) || !ValidaDorsal(Dorsal)) return;
            
            AddToListaJugadores(_textConverter.ConvertText(Nombre + " " + Apellido + ": Dorsal " + Dorsal));
            Nombre = string.Empty;
            Apellido = string.Empty;
            Dorsal = 0;
        }

        public void AddToListaJugadores(string item)
        {
            if (!_jugadores.Contains(item))
                _jugadores.Add(item);
        }

        
    }
}
